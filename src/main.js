import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
Vue.use(Router)

//import FicEdit from './components/ficEdit/FicEdit.vue'
import Logs from './components/system/Logs.vue'
import Configs from './components/system/Configs.vue'
import Program from './components/Program.vue'
import SerieSelector from './components/SerieSelector.vue'
import SerieEditor from './components/SerieEditor.vue'
import TempsForts from './components/TempsForts.vue'
import PigeText from './components/system/PigeText.vue'
import PigeOgg from './components/system/PigeOgg.vue'
import Uploader from './components/Uploader.vue'
import ImportState from './components/ImportState.vue'
import CustomStatic from './components/CustomStatic.vue'
import PagesStatic from './components/PagesStatic.vue'
import Playlist from './components/Playlist.vue'
import FileDownloader from './components/FileDownloader.vue'
import ReplaysManager from './components/ReplaysManager.vue'
import DirectFile from './components/DirectFile.vue'
import PigeConfig from './components/PigeConfig.vue'
import Users from './components/Users.vue'
import DashBoard from './components/DashBoard.vue'
import Programation from './components/Programation.vue'
import EmissionManager from './components/EmissionManager.vue'
import OggPigeExtractor from './components/OggPigeExtractor.vue'

Vue.prototype.$user = {'jwt': ''}

const router = new Router({
    routes: [
        {
            path: '/',
            component: DashBoard,
        },
        {
            path: '/upload/:type',
            component: Uploader,
            //component: Dashboard,
            props: true,
        },
        {
            path: '/download',
            component: FileDownloader,
        },
        {
            path: '/logs',
            component: Logs,
        },
        {
            path: '/config',
            component: Configs,
        },
        {
            path: '/import-state',
            component: ImportState,
        },
        {
            path: '/program',
            component: Program,
            props: process.env.NODE_ENV == 'development' ? {baseUrl: ''} : {baseUrl: ''}
        },
        {
            path: '/playlist',
            component: Playlist,
        },
        {
            path: '/directfile',
            component: DirectFile,
        },
        {
            path: '/programation/tops',
            component: Programation,
            props: {prefix:'_TopHoraire-'},
        },
        {
            path: '/programation/emissions',
            component: Programation,
            props: {prefix:'_Grille-', types:['lst', 'wavM']},
        },
        {
            path: '/programation/jinglesContextuels',
            component: Programation,
            props: {prefix:'_Jingle-', hours:['Matin', 'Aprem', 'Soir', 'Nuit']},
        },
        {
            path: '/programation/liens',
            component: Programation,
            props: {prefix:'_Programmation-', days:['_'], hours:['JingleDefaut', 'JingleContextuel']},
        },
        {
            path: '/programation/playlists',
            component: Programation,
            props: {prefix:'_Bourrage-', types:['lst'], hours:['Matin', 'Aprem', 'Soir', 'Nuit']},
        },
        {
            path: '/series/PodcastsStatiques',
            component: SerieSelector,
            props: {
                forcedFamily: true,
                defaultFamilyIndex: 'PodcastsStatiques',
				forcedPrefix: 'Serie',
				title: 'Séries audio',
				onlyText: false,
            },
        },
        {
            path: '/series/Categories',
            component: SerieSelector,
            props: {
                forcedFamily: true,
                defaultFamilyIndex: 'Categories',
				title: 'Catégories',
				onlyText: true,
            },
        },
        {
			path: '/serie/:serieSimpleName/:onlyText',
            component: SerieEditor,
            props: (route) => ({
				serieSimpleName: route.params.serieSimpleName,
				onlyText: route.params.onlyText == "true",
			}),
        },
        {
            path: '/tempsforts',
            component: TempsForts,
            props: process.env.NODE_ENV == 'development' ? {baseUrl: ''} : {baseUrl: ''}
        },
        {
            path: '/replays',
            component: ReplaysManager,
        },
        {
            path: '/pigetext',
            component: PigeText,
        },
        {
            path: '/pigeogg',
            component: PigeOgg,
        },
        {
            path: '/extract-pige',
            component: OggPigeExtractor,
        },
        {
            path: '/emissions',
            component: EmissionManager,
        },
        {
            path: '/customize',
            component: CustomStatic,
        },
        {
            path: '/PagesStatiques',
            component: PagesStatic,
        },
        {
            path: '/pigeconfig',
            component: PigeConfig,
        },
        {
            path: '/users',
            component: Users,
        },
    ]
})

new Vue({
  el: '#app',
  render: h => h(App),
  router
})

require('./assets/css/noscript.css')
require('./assets/css/main.css')


export {OmaItemFileCommon, OmaItemSound, OmaItemFile, OmaItemImage, OmaItemText, OmaItemTimedReference}

// TODO vérifier le titre/auteur

class OmaItemFileCommon {
    constructor () {
        this.title = ''
        this.author = ''
        this.licence= ''
        this.description = ''
        this.file = null
        this.existsInBase = null /* null=unknown, false=no, 'new'=justUploaded 'sending'=uploading */
    }
    isValid() {
        if (this.title === '' || this.author === '') {
            alert('Les champs titre et auteur doivent être renseignés.')
            return false
        }
        return true
    }
    toFic (baseFic={}) {
        if (!this.isValid()) { return null }
        baseFic['Title'] = this.title
        baseFic['Author'] = this.author
        baseFic['Licence'] = this.licence
        baseFic['Description'] = this.description
        return baseFic
    }
    getFilename () {
        return this.title + '-' + this.author
    }
    getFicMsg () {
        if (!this.isValid()) { return false }
        return {
            'ref': 11,
            'content': this.getFilename(),
        }
    }
    getShortString () {
        return this.title + ' de ' + this.author
    }
    send (callback, ficItems={}) {
        if (this.existsInBase) { return }
        var ficFields = this.toFic(ficItems)
        if (!ficFields) { return }
        var formData = new FormData();
        formData.append('ficJson', JSON.stringify(ficFields))
        formData.append('file', this.file)
        var request = new XMLHttpRequest();
        request.open("POST", "http://localhost:5000/quick/");
        request.send(formData);
        this.existsInBase = 'sending'
        var _this = this
        request.onload = () => {
            _this.existsInBase = 'new'
            if (callback) { callback() }
        }
        request.onerror = (e) => {
            alert('L’envoi du fichier '+_this.title+' a échoué…')
            console.error(e)
            _this.existsInBase = false
        }
        request.onabort = request.onerror
    }
}

class OmaItemSound extends OmaItemFileCommon {
    constructor () {
        super()
        this.htmlSectionTitle = 'Ajouter un son'
        this.quality = ''
    }
    toFic (baseFic={}) {
        var fic = super.toFic(baseFic)
        if (!fic) { return; }
        fic['WavQ'] = this.quality
        return fic
    }
}

class OmaItemFile extends OmaItemFileCommon {
    constructor () {
        super()
        this.htmlSectionTitle = 'Ajouter un autre fichier'
    }
}

class OmaItemImage extends OmaItemFileCommon {
    constructor () {
        super()
        this.htmlSectionTitle = 'Ajouter une image'
    }
}

class OmaItemText {
    constructor () {
        this.htmlSectionTitle = 'Ajouter un texte'
        this.content = ''
    }
    isValid () {
        if (this.content == '') {
            alert('Le texte ne doit pas être vide !')
            return false
        }
        return true
    }
    getFicMsg () {
        if (!this.isValid()) { return false }
        return {
            'ref' : 11,
            'content' : this.content,
        }
    }
    getShortString () {
        return this.content.slice(0,20) + (this.content.length >20 ? '…' : '')
    }
}


/* This class contain an object type and the actual object */
class OmaItemTimedReference {
    constructor (fixed_type=null) {
        this._valid_types = { /* Drop-down list text and class to instanciate */
             OmaItemSound: {'text': 'Audio', 'object':OmaItemSound},
             OmaItemImage: {'text': 'Image', 'object':OmaItemImage},
             OmaItemFile: {'text': 'Autre fichier', 'object':OmaItemFile},
             OmaItemText: {'text': 'Texte', 'object':OmaItemText},
        }
        this.beginTime = 0
        this.duration = 300
        this._type = null
        this._fixed_type = fixed_type
        this._type = fixed_type
        this.objects = {}
        this.editing = true
        for (var type in this._valid_types) {
            this.objects[type] = new this._valid_types[type].object()
        }
    }
    isNew () {
        if (this._type == null) return false
        return this.getObject().existsInBase == 'new'

    }
    getFicMsg () {
        if (this.duration == null || this.beginTime == null) {
            alert('La durée et le temps de début doivent être renseignés')
            return
        }
        var ficMsg = this.getObject().getFicMsg()
        if (!ficMsg) { return }
        ficMsg['duration'] = this.duration
        ficMsg['beginTime'] = this.beginTime
        return ficMsg
    }
    getObject () {
        return this.objects[this._type]
    }
    getTypeString () {
        return this._valid_types[this._type].text
    }
}




/*
 * COMPONENTSs
 */











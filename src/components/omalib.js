/*
 * Convert a prg file in text format to a js object
 * {id:0, mode: 'H', year:0, month:0, day:0, hour:0, minute:0, second:0, action:'setvar', params: [0, 'Demo']},
 */
export function loadOmaPrg (text) {
    var program = []
    var id = 0
    var lines = text.split('\n')
    var mode = ''
    for (var i in lines) {
        var line = lines[i].trim()
        if (line.startsWith('#')) continue
        if (line.startsWith('ProgrammeSynchro')) {
            mode = line.split(' ')[1]
            continue
        }
        var elements = line.split(' ')
        program.push({
            id:id,
            mode:elements[0],
            year:elements[1],
            month:elements[2],
            day:elements[3],
            hour:elements[4],
            minute:elements[5],
            second:elements[6],
            action:elements[7],
            params: elements.slice(8,elements.length),
        })
        id += 1
    }
    return {mode: mode, program:program}
}

